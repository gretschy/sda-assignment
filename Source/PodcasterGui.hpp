//
//  PodcasterGui.hpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#ifndef PodcasterGui_hpp
#define PodcasterGui_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Podcaster.hpp"
#include "Audio.hpp"

class PodcasterGui : public Component,
                            Button::Listener,
                            Slider::Listener,
                            Timer,
                            ChangeListener
{
public:
    /**Constructor*/
    PodcasterGui(Podcaster& podCaster_);
    
    /**Destructor*/
    ~PodcasterGui();
    
    /**Called when a button is clicked*/
    void buttonClicked(Button* button) override;
    
    /**Called when a slider value changes*/
    void sliderValueChanged(Slider* slider) override;
    
    /**Timer callback*/
    void timerCallback() override;
    
    /**Listener for the recording thumbnail*/
    void changeListenerCallback(ChangeBroadcaster* source) override;
    
    /**Called when the component is resized*/
    void resized() override;
    
    /**Draws gui shapes*/
    void paint(Graphics& graphics_) override;
    
    
private:
    /**Audio*/
    Audio audio;
    AudioFormatManager formatManager;
    
    /**Gui components*/
    Podcaster& podCaster;
    TextButton recordButton;
    //TextButton saveButton;
    TextButton playbackButton;
    Slider playbackSlider;
    Colour colour;
    
    /**Recording thumbnail*/
    AudioThumbnailCache recordingThumbCache;
    AudioThumbnail recordingThumbnail;
    bool displayFullThumb;

};



#endif /* PodcasterGui_hpp */
