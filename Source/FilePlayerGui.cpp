//
//  FilePlayerGui.cpp
//  JuceBasicWindow
//
//  Created by Tom on 08/12/2016.
//
//

#include "FilePlayerGui.hpp"

FilePlayerGui::FilePlayerGui(FilePlayer& filePlayer_) : filePlayer(filePlayer_), thumbCache(1), fileThumbnail(512, formatManager, thumbCache)
{
    playbackButton.setButtonText("PLAY");
    playbackButton.setColour(TextButton::buttonColourId, Colours::darkslateblue);
    playbackButton.setColour(TextButton::textColourOffId, Colours::antiquewhite);
    addAndMakeVisible(&playbackButton);
    playbackButton.addListener(this);

    playbackPositionSlider.setSliderStyle(Slider::LinearHorizontal);
    playbackPositionSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    playbackPositionSlider.setRange(0.0, 1.0);
    addAndMakeVisible(&playbackPositionSlider);
    playbackPositionSlider.addListener(this);
    
    playBackPositionSliderLabel.setText("File Scrubbing", dontSendNotification);
    playBackPositionSliderLabel.setColour(Label::textColourId, Colours::antiquewhite);
    addAndMakeVisible(playBackPositionSliderLabel);
    
    gainSlider.setSliderStyle(Slider::Rotary);
    gainSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    gainSlider.setColour(Slider::rotarySliderFillColourId, Colours::antiquewhite);
    gainSlider.setColour(Slider::rotarySliderOutlineColourId, Colours::antiquewhite);
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(0.1);
    addAndMakeVisible(&gainSlider);
    gainSlider.addListener(this);
    
    gainSliderLabel.setText("Volume", dontSendNotification);
    gainSliderLabel.setColour(Label::textColourId, Colours::antiquewhite);
    addAndMakeVisible(gainSliderLabel);
    
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    fileThumbnail.addChangeListener(this);
}

FilePlayerGui::~FilePlayerGui()
{
    fileThumbnail.clear();
    fileThumbnail.removeChangeListener(this);
    delete fileChooser;
}

void FilePlayerGui::buttonClicked(Button* button)
{
    if (button == &playbackButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true)
        {
            playbackButton.setButtonText("STOP");
            playbackButton.setColour(TextButton::buttonColourId, Colours::lightgreen);
            startTimer(250);
        }
        
        else if (filePlayer.isPlaying() == false)
        {
            playbackButton.setButtonText("PLAY");
            playbackButton.setColour(TextButton::buttonColourId, Colours::darkslateblue);
            stopTimer();
        }
    }
}

void FilePlayerGui::sliderValueChanged(Slider* slider)
{
    filePlayer.setPosition(playbackPositionSlider.getValue());
    filePlayer.setGain(gainSlider.getValue());
}

void FilePlayerGui::timerCallback()
{
    playbackPositionSlider.setValue(filePlayer.getPosition());
}

void FilePlayerGui::resized()
{
    playbackPositionSlider.setBounds(0, 60, 300, 40);
    playBackPositionSliderLabel.setBounds(25, 40, 100, 50);
    fileChooser->setBounds (150, 15, 200, 30);
    playbackButton.setBounds (0, 10, 140, 40);
    gainSlider.setBounds(400, 10, 40, 40);
    gainSliderLabel.setBounds(393, 50, 60, 50);
}

void FilePlayerGui::paint (Graphics& graphics_)
{
    graphics_.fillAll (Colours::darkgrey);
    graphics_.setColour (Colours::darkslateblue);
    
    if (fileThumbnail.getTotalLength() > 0.0)
    {
        const double endTime = displayFullThumb ? fileThumbnail.getTotalLength()
        : jmax (30.0, fileThumbnail.getTotalLength());
        
        Rectangle<int> thumbArea (getLocalBounds());
        fileThumbnail.drawChannels (graphics_, thumbArea.reduced (2), 0.0, endTime, 0.5f);
        
    }
    else
    {
        graphics_.setFont (14.0f);
        graphics_.drawFittedText ("(No file loaded)", getLocalBounds(), Justification::centred, 2);
    }
}

void FilePlayerGui::filenameComponentChanged(FilenameComponent* filenameComponentThatHasChanged)
{
    if (filenameComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            filePlayer.loadFile(audioFile);
            fileThumbnail.setSource(new FileInputSource (audioFile));
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "File load failure",
                                         "Cannot open file\n\n");
        }
    }
}

void FilePlayerGui::changeListenerCallback (ChangeBroadcaster* source)
{
    if (source == &fileThumbnail)
    {
        repaint();
    }
}
