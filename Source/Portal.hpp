//
//  Portal.hpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#ifndef Portal_hpp
#define Portal_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


class Portal : public Component,
                      Button::Listener,
                      Image
{
public:
    
    /**Constructor*/
    Portal();
    
    /**Destructor*/
    ~Portal();
    
    void buttonClicked(Button* button) override;
    void resized() override;
    
private:
    ImageButton portalImageButton;
    Image image1;
    
};

#endif /* Portal_hpp */