//
//  FilePlayerGui.hpp
//  JuceBasicWindow
//
//  Created by Tom on 08/12/2016.
//
//

#ifndef FilePlayerGui_hpp
#define FilePlayerGui_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.hpp"

class FilePlayerGui : public Component,
                            Button::Listener,
                            Slider::Listener,
                            Timer,
                            FilenameComponentListener,
                            ChangeListener

{
public:
    /**Constructor*/
    FilePlayerGui(FilePlayer& filePlayer_);
    /**Destructor*/
    ~FilePlayerGui();
    
    /**Called when a button is clicked*/
    void buttonClicked(Button* button) override;
    
    /**Called when a slider value changes*/
    void sliderValueChanged(Slider* slider) override;
    
    /**Timer callback function*/
    void timerCallback() override;
    
    /**Called when the component resizes*/
    void resized() override;
    
    /**Handles the graphics*/
    void paint(Graphics& graphics_) override;
    
    /**Called when the selected file in the player changes*/
    void filenameComponentChanged(FilenameComponent* filenameComponentThatHasChanged) override;
    
    /**Listener for the audio file thumbnail*/
    void changeListenerCallback (ChangeBroadcaster* source) override;

private:
    /**Audio*/
    AudioFormatManager formatManager;
//    ScopedPointer<AudioFormatReaderSource> readerSource;
//    AudioTransportSource transportSource;
//    TransportState state;
    
    /**Gui components*/
    TextButton playbackButton;
    Slider playbackPositionSlider;
    Label playBackPositionSliderLabel;
    Slider gainSlider;
    Label gainSliderLabel;
    FilenameComponent* fileChooser;
    FilePlayer& filePlayer;
    
    /**File thumbnail*/
    AudioThumbnailCache thumbCache;
    AudioThumbnail fileThumbnail;
    bool displayFullThumb;

    
};


#endif /* FilePlayerGui_hpp */
