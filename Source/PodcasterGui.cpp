//
//  PodcasterGui.cpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#include "PodcasterGui.hpp"

PodcasterGui::PodcasterGui(Podcaster& podCaster_) : podCaster(podCaster_), recordingThumbCache(1), recordingThumbnail(512, formatManager, recordingThumbCache), displayFullThumb(false)
{
    recordButton.setButtonText("RECORD");
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::crimson);
    recordButton.setColour(TextButton::textColourOffId, Colours::white);
    addAndMakeVisible(&recordButton);
    recordButton.addListener(this);
    
    playbackButton.setButtonText ("PLAY");
    playbackButton.setColour(TextButton::buttonColourId, Colours::lightgreen);
    playbackButton.setColour(TextButton::buttonOnColourId, Colours::green);
    playbackButton.setColour(TextButton::textColourOffId, Colours::white);
    addAndMakeVisible (&playbackButton);
    playbackButton.addListener (this);
    
    formatManager.registerBasicFormats();
    recordingThumbnail.addChangeListener(this);
}

PodcasterGui::~PodcasterGui()
{
    recordingThumbnail.removeChangeListener(this);
}

void PodcasterGui::buttonClicked(Button* button)
{
    if (button == &recordButton && podCaster.getRecordingState() == false)
    {
        podCaster.setRecordingState(true);
        //podCaster.setRecordingState(!podCaster.getRecordingState());
        recordButton.setToggleState(podCaster.getRecordingState(), dontSendNotification);
        
        //audio.writeMixedOutput();
        startTimer(100);
    }
    
    else if (button == &recordButton && podCaster.getRecordingState() == true)
    {
        podCaster.setRecordingState(false);
        stopTimer();
    }
    

    else if (button == &playbackButton)
    {
        podCaster.setPlaybackState(!podCaster.getPlaybackState());
        if (podCaster.getPlaybackState() == true)
        {
            playbackButton.setButtonText("Stop");
        }
        
        else
            playbackButton.setButtonText("Play");
    }
    
}

void PodcasterGui::sliderValueChanged(Slider* slider)
{

}

void PodcasterGui::timerCallback()
{
    
}

void PodcasterGui::resized()
{
    recordButton.setBounds(225, 100, 200, 80);
    playbackButton.setBounds(225, 300, 200, 80);
    
}

void PodcasterGui::paint(Graphics& graphics_)
{
//    graphics_.drawEllipse(450, 120, 40, 40, 4);
//    graphics_.fillEllipse(450, 120, 40, 40);
//    graphics_.setColour(Colours::crimson);
    
    graphics_.fillAll (Colours::lightgrey);
    graphics_.setColour (Colours::lightsteelblue);
    
    if (recordingThumbnail.getTotalLength() > 0.0)
    {
        const double endTime = displayFullThumb ? recordingThumbnail.getTotalLength()
        : jmax (30.0, recordingThumbnail.getTotalLength());
        
        Rectangle<int> thumbArea (getLocalBounds());
        recordingThumbnail.drawChannels (graphics_, thumbArea.reduced (2), 0.0, endTime, 1.0f);
    }
    else
    {
        graphics_.setFont (14.0f);
        graphics_.drawFittedText ("(No file recorded)", getLocalBounds(), Justification::centred, 2);
    }
}

void PodcasterGui::changeListenerCallback (ChangeBroadcaster* source)
{
    if (source == &recordingThumbnail)
    {
        repaint();
    }
}

