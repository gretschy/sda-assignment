/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "PodcasterGui.hpp"
#include "Portal.hpp"
#include "FilePlayerGui.hpp"
#include "FilePlayer.hpp"



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component
//                               MenuBarModel
{
public:
    //==============================================================================
    MainComponent(Audio& audio_);
    
    ~MainComponent();

    void resized() override;
    
//    //MenuBarEnums/Callbacks========================================================
//    enum Menus
//    {
//        FileMenu=0,
//        
//        NumMenus
//    };
//    
//    enum FileMenuItems
//    {
//        AudioPrefs = 1,
//        
//        NumFileItems
//    };
//    StringArray getMenuBarNames() override;
//    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
//    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Portal portal;
    Audio& audio;
    PodcasterGui podcasterGui;
    OwnedArray<FilePlayerGui> filePlayerGuis;
    const static int numFilePlayerGuis = 6;
    //const static int numPodcasterGuis = 1;
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
