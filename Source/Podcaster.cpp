//
//  Podcaster.cpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#include "Podcaster.hpp"

Podcaster::Podcaster()
{
    recordingState = false;
    playbackState = false;
    bufferPosition = 0;
    
    audioSampleBuffer.setSize(2, bufferSize);
}

Podcaster::~Podcaster()
{
    audioSampleBuffer.clear();
}

void Podcaster::setRecordingState(bool newRecordingState)
{
    recordingState = newRecordingState;
}

bool Podcaster::getRecordingState() const
{
    return recordingState.get();
}

void Podcaster::setPlaybackState(bool newPlaybackState)
{
    playbackState = newPlaybackState;
}

bool Podcaster::getPlaybackState() const
{
    return playbackState.get();
}

double Podcaster::processSample (float audioInput)
{
    float output = 0.0;
    if (getPlaybackState() == true)
    {
        //play
        output = audioSampleBuffer.getSample(0, bufferPosition);
        //click test = 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 2)) == 0)
            output += 0.25f;
        
        //record
        if (getRecordingState()== true)
        {
            float* audioSample;
            
            audioSample = audioSampleBuffer.getWritePointer(0,bufferPosition);
            *audioSample += audioInput;
            
        }
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    
    //send audio output to IOMixing
    //podcasterOutput->setMixedOutput(output);
    
    
    return output;
}

//void Podcaster::write()
//{
//    FileChooser chooser ("Please select output location...",
//                         File::getSpecialLocation(File::userDesktopDirectory), ("*.wav"));
//
//    if (chooser.browseForFileToSave(true))
//    {
//        File file (chooser.getResult().withFileExtension(".wav"));
//        OutputStream* outStream = file.createOutputStream();
//        WavAudioFormat wavFormat;
//        AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 1, 16, NULL, 0);
//        writer -> writeFromAudioSampleBuffer(audioSampleBuffer, 0, audioSampleBuffer.getNumSamples());
//
//        delete writer;
//    }
//}

int Podcaster::getBufferSize()
{
    return bufferSize;
}


