//
//  Portal.cpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#include "Portal.hpp"

Portal::Portal()
{
    File imageFile = File("/Users/Tom/Pictures/Film/F3/tomxrg2003.12.16029.jpg");
    image1 = juce::ImageFileFormat::loadFrom(imageFile);
    portalImageButton.setImages(true, true, true, image1, 0.5f, Colours::transparentBlack, image1, 1.0f, Colours::transparentWhite, image1, 1.0f, Colours::aliceblue);
    
    addAndMakeVisible(portalImageButton);
}

Portal::~Portal()
{
    
}

void Portal::buttonClicked(Button* button)
{
    if (button == &portalImageButton)
    {
        //do some stuff
    }
}

void Portal::resized()
{
    portalImageButton.setBounds(0, 0, 500, 500);
}