//
//  IOMixing.hpp
//  JuceBasicWindow
//
//  Created by Tom on 20/12/2016.
//
//

#ifndef IOMixing_hpp
#define IOMixing_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.hpp"
#include "Podcaster.hpp"


class IOMixing
{
public:
    
    /**Constructor*/
    IOMixing();
    
    /**Destructor*/
//    ~IOMixing();
    
    /**Inputs are sent here to be mixed*/
    void setMixedOutput(float podcasterInput);
    
    /**Returns mixed inputs as single output*/
    float getMixedOutput();
    
    
private:
    //Podcaster podcaster;
    FilePlayer filePlayer;
    float masterOutput;
    
};

#endif /* IOMixing_hpp */
