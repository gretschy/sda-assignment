//
//  Audio.cpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#include "Audio.hpp"

Audio::Audio()
{
        for (int i=0; i < numFilePlayers; i++)
        {
            mixerSource.addInputSource(&getFilePlayer(i), false);
        }
    
    /**intialise the audio device manager*/
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    /**load the 'INSERT FILEPLAYER OR SIMILAR HERE' into the audio source*/
    audioSourcePlayer.setSource (&mixerSource); //not sure this is right??
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    
    audioSourcePlayer.setSource (nullptr);
    mixerSource.removeAllInputs();
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    /**MIDI inputs arrive here*/
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    /** get the audio from file player - player puts samples in the output buffer*/
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    /** audio processing is done here*/
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float inSampL;
    float inSampR;
    
    while(numSamples--)
    {
//        float podcasterOutput = 0.f;
//        
//        podcasterOutput = podcaster.processSample(*inL) + podcaster.processSample(*inR);
        
        inSampL = *outL;
        inSampR = *outR;
        
        *outL = inSampL * 1.0;
        *outR = inSampR * 1.0;
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


//void Audio::writeMixedOutput()
//{
//            FileChooser chooser ("Please select output location...",
//                             File::getSpecialLocation(File::userDesktopDirectory), ("*.wav"));
//        
//        if (chooser.browseForFileToSave(true))
//        {
//            File file (chooser.getResult().withFileExtension(".wav"));
//            OutputStream* outStream = file.createOutputStream();
//            WavAudioFormat wavFormat;
//            AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 1, 16, NULL, 0);
//            //AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 2, 16, nullptr, 0);//test line
//            writer->writeFromAudioSampleBuffer(audioSampleBuffer, 0, audioSampleBuffer.getNumSamples());
//            //writer->writeFromAudioSource(mixerSource, 0, audioSampleBuffer.getNumSamples());
//            
//            
//            delete writer;
//        }
//}

void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}

