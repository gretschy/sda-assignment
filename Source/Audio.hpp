//
//  Audio.hpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#ifndef Audio_hpp
#define Audio_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.hpp"
#include "Podcaster.hpp"
#include "IOMixing.hpp"


class Audio :   public MidiInputCallback,
                       AudioIODeviceCallback

{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /**Returns FilePlayer objects for the gui to control*/
    FilePlayer& getFilePlayer(int index) {return filePlayer[index];}
    
    /**Returns a Podcaster object for the gui to control*/
    Podcaster& getPodcaster() {return podcaster;}
    
    /**Handles incoming MIDI input*/
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /**Callback function handling audio I/O*/
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /**Notifies audio device start*/
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /**Notifies audio device end*/
    void audioDeviceStopped() override;
    
    /**Writes the output of the fileplayers and podcaster to a single file*/
    //void writeMixedOutput();
    
    
private:
    static const int numFilePlayers = 6;
    FilePlayer filePlayer[numFilePlayers];
    Podcaster podcaster;
    //AudioSampleBuffer audioSampleBuffer;
    AudioDeviceManager audioDeviceManager;
    AudioSourcePlayer audioSourcePlayer;
    MixerAudioSource mixerSource;
   
    

};

#endif /* Audio_hpp */