//  Podcaster.hpp
//  JuceBasicWindow
//
//  Created by Tom on 06/12/2016.
//
//

#ifndef Podcaster_hpp
#define Podcaster_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.hpp"
#include "IOMixing.hpp"

class Podcaster
{
public:
    
    /**Constructor*/
    Podcaster();
    
    /**Destructor*/
    ~Podcaster();
    
    /**sets the recording state of the podcaster*/
    void setRecordingState(bool newRecordState);
    
    /**returns the current recording state of the podcaster*/
    bool getRecordingState() const;
    
    /**sets the playback state of the podcaster*/
    void setPlaybackState(bool newPlaybackState);
    
    /**returns the playback state of the podcaster*/
    bool getPlaybackState() const;
    
    /**processes audio by sample*/
    double processSample(float audioInput);
    
    /**write buffer output to .wav file*/
    //void write();
    
    /**returns buffer size value*/
    int getBufferSize();
    
    
private:
    Atomic<int> recordingState;
    Atomic<int> playbackState;
    
    /**audio*/
    static const int bufferSize = 441000; //10 seconds
    unsigned int bufferPosition;
    AudioSampleBuffer audioSampleBuffer;
    //    IOMixing* podcasterOutput;
    
};

#endif /* Podcaster_hpp */
