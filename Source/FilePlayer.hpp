//
//  FilePlayer.hpp
//  JuceBasicWindow
//
//  Created by Tom on 08/12/2016.
//
//

#ifndef FilePlayer_hpp
#define FilePlayer_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Simple FilePlayer class - streams audio from a file.
 */
class FilePlayer : public AudioSource

{
public:
    /**Constructor*/
    FilePlayer();
    
    /**Destructor*/
    ~FilePlayer();
    
    /**Starts or stops playback of the looper*/
    void setPlaying (const bool newState);
    
    /**Gets the current playback state of the looper*/
    bool isPlaying () const;
    
    /**Loads the specified file into the transport source*/
    void loadFile(const File& newFile);
    
    /**sets the transport position from the postion of a slider*/
    void setPosition(float newPosition);
    
    /**returns the current position of the transport source*/
    float getPosition();
    
    /**sets playback rate of the file*/
    void setPlaybackRate(float newRate);
    
    /**sets gain level of the file*/
    void setGain(float newGain);
    
    /**returns gain level of file*/
    float getGain();
    
    
    //AudioSource
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
private:
    AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
    AudioTransportSource audioTransportSource;	// this controls the playback of a positionable audio stream, handling the
    // starting/stopping and sample-rate conversion
    TimeSliceThread thread;                 //thread for the transport source
    ResamplingAudioSource resamplingAudioSource;
    float gain;
};

#endif /* FilePlayer_hpp */
