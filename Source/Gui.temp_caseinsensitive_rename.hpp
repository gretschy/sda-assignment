//
//  GUI.hpp
//  JuceBasicWindow
//
//  Created by Tom on 08/12/2016.
//
//

#ifndef GUI_hpp
#define GUI_hpp

#include <stdio.h>

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.hpp"

class GUI : public Component,
                   Button::Listener,
                   Slider::Listener,
                   Timer,
                   FilenameComponentListener
{
public:
    
    /**Constructor*/
    GUI();
    
    /**Destructor*/
    ~GUI();
    
    void buttonClicked(Button* button) override;
    void sliderValueChanged(Slider* slider) override;
    void timerCallback() override;
    
    
private:
    TextButton loadButton;
    TextButton playbackButton;
    Slider playbackPositionSlider;
    Slider gainSlider;
    FilePlayer& filePlayer;
    
};


#endif /* GUI_hpp */
