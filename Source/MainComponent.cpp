/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent(Audio& audio_) : audio (audio_), podcasterGui(audio.getPodcaster())

{
    PodcasterGui(audio.getPodcaster());
    addAndMakeVisible(podcasterGui);
    
    for (int i=0; i < numFilePlayerGuis; i++)
    {
        filePlayerGuis.add(new FilePlayerGui(audio.getFilePlayer(i)));
        addAndMakeVisible(filePlayerGuis[i]);
        DBG(filePlayerGuis.size());
    }
    //addAndMakeVisible(portal);
    
    setSize (1200, 700);
    
}

MainComponent::~MainComponent()
{
    filePlayerGuis.removeLast();
}

void MainComponent::resized()
{
    int width = getWidth();
    int height = getHeight();
    
    for(int i=0; i < numFilePlayerGuis; i++)
    {
        filePlayerGuis[i]->setSize(width/numFilePlayerGuis, height/numFilePlayerGuis);
        filePlayerGuis[i]->setBounds(0, ((height/numFilePlayerGuis) * i), width/2, height/numFilePlayerGuis);
    }
    
    podcasterGui.setSize(width/2, height/2);
    podcasterGui.setBounds(600, 0, width/2, height);
    
}

//StringArray MainComponent::getMenuBarNames()
//{
//    const char* const names[] = { "File", 0 };
//    return StringArray (names);
//}
//
//PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
//{
//    PopupMenu menu;
//    if (topLevelMenuIndex == 0)
//        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
//    return menu;
//}
//
//void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
//{
//    if (topLevelMenuIndex == FileMenu)
//    {
//        if (menuItemID == AudioPrefs)
//        {
//            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
//                                                            0, 2, 2, 2, true, true, true, false);
//            audioSettingsComp.setSize (450, 350);
//            DialogWindow::showModalDialog ("Audio Settings",
//                                           &audioSettingsComp, this, Colours::lightgrey, true);
//        }
//    }
//}